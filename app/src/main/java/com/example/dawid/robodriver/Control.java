package com.example.dawid.robodriver;
//wykorzystywane bibliotek
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.UUID;

public class Control extends AppCompatActivity {

    Button ActGrip, Discon, ActArm1, ActArm2, ActArm3, ActBasis, SteerBtn, StraigthBtn, BackBtn, ResetBtn, SlowBtn, FastBtn; //definicja zmiennych odpiwadających za przyciski
    SeekBar GripAngle, Arm1Angle, Arm2Angle, Arm3Angle, BasisAngle, SteerAngle; //definicja zmiennych odpowiadających za suwaki
    String address = null; //zmienna przechowująca adres
    TextView lev, lev1, lev2, lev3, lev4, lev5; //zmienne do przechowywania wartości pozycji suwaka
    private ProgressDialog progress; // obkiet niezbędny do wyświetlania paska postępu połączenia
    BluetoothAdapter mBluetooth = null; //definicja bluetooth adaptera
    BluetoothSocket BTsocket = null; //definicja gniazdka
    private boolean IsBTConnected = false; //flaga do sygnalizacji czy połączenie się zakończyło sukcesem
    boolean ActGripflag=false; //flagi do sygnalizacji aktualnie sterowanej części ramienia
    boolean ActArm1flag=false;
    boolean ActArm2flag=false;
    boolean ActArm3flag=false;
    boolean ActBasisflag=false;
    boolean SteerFlag=false; // flaga do sygnalizacji sterowania osią skrętną



    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"); //identyfikator urządzenia


    @SuppressLint("ClickableViewAccessibility")
    @android.support.annotation.RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);
        //uzyskanie adresu urządzenia
        Intent newInt = getIntent();
        address = newInt.getStringExtra(Device_List.EXTRA_ADDRESS);
        // połączenie zmiennych z przyciskami dodanymi w app designerze
        ActGrip = (Button) findViewById(R.id.button2);
        ActArm1 = (Button) findViewById(R.id.button3);
        ActArm2 = (Button) findViewById(R.id.button5);
        ActArm3 = (Button) findViewById(R.id.button9);
        ActBasis = (Button) findViewById(R.id.button11);

        SteerBtn = (Button) findViewById(R.id.button13);
        SlowBtn =(Button) findViewById(R.id.button14);
        FastBtn =(Button) findViewById(R.id.button18);
        StraigthBtn = (Button) findViewById(R.id.button15);
        BackBtn = (Button) findViewById(R.id.button16);
        ResetBtn = (Button) findViewById(R.id.button17);

        Discon = (Button) findViewById(R.id.button4);
        // połączenie zmmienych z dodanymi w app designerze suwakami
        GripAngle = (SeekBar) findViewById(R.id.seekBar2);
        Arm1Angle = (SeekBar) findViewById(R.id.seekBar);
        Arm2Angle = (SeekBar) findViewById(R.id.seekBar3);
        Arm3Angle = (SeekBar) findViewById(R.id.seekBar4);
        BasisAngle = (SeekBar) findViewById(R.id.seekBar5);
        SteerAngle = (SeekBar) findViewById(R.id.seekBar7);
        // ustawienie wartości maksymalnych dla suwaków
        GripAngle.setMax(160);
        GripAngle.setMin(50); //ograniczenie ruchu chwytaka ze względu na jego konstrukcję
        Arm1Angle.setMax(180);
        Arm2Angle.setMax(180);
        Arm3Angle.setMax(180);
        BasisAngle.setMax(180);
        SteerAngle.setMax(80);
        //połączenie zmiennych przechowujących pozycje suwaka z polami tekstowymi interfejsu
        lev = (TextView) findViewById(R.id.lev);
        lev1 = (TextView)findViewById(R.id.lev1);
        lev2 = (TextView)findViewById(R.id.lev2);
        lev3 = (TextView)findViewById(R.id.lev3);
        lev4 = (TextView)findViewById(R.id.lev4);
        lev5 = (TextView)findViewById(R.id.lev5);


        new ConnectBT().execute(); //wykonanie funkcji nawiązania połączenia z innymi urządzeniem bluetooth

        ActGrip.setOnClickListener(new View.OnClickListener() { //funkcja wykonywująca się podczas naciśniecia przycisku przez użytkownika
            @Override
            public void onClick(View v) {
                TurnONGrip(); //wywołanie funkcji wysłającej litery "GO" do sterownika
                ActGripflag =true;// ustawienie flagi chwytaka
                ActArm1flag=false; //pozostałe flagi elementów ustawione na false aby nie zakłócać sterowania chwytakiem
                ActArm2flag=false;
                ActArm3flag=false;
                ActBasisflag=false;
                SteerFlag=false;
            }
        });


        Discon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Disconnect(); //wywołanie funkcji rozłączenia
            }
        });

        ActArm1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TurnONArm1();
                ActArm1flag =true;
                ActGripflag =false;
                ActArm2flag=false;
                ActArm3flag=false;
                ActBasisflag=false;
                SteerFlag=false;
            }
        });

        ActArm2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TurnONArm2();
                ActArm2flag= true;
                ActGripflag =false;
                ActArm1flag=false;
                ActArm3flag=false;
                ActBasisflag=false;
                SteerFlag=false;
            }
        });


        ActArm3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TurnONArm3();
                ActArm3flag= true;
                ActGripflag=false;
                ActArm1flag=false;
                ActArm2flag=false;
                ActBasisflag=false;
                SteerFlag=false;
            }
        });


        ActBasis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TurnONBasis();
                ActBasisflag=true;
                ActGripflag =false;
                ActArm1flag=false;
                ActArm2flag=false;
                ActArm3flag=false;
                SteerFlag=false;
            }
        });


        SteerBtn.setOnClickListener(new View.OnClickListener() { //wykonaj funkcję po naciśnieciu przycisku
            @Override
            public void onClick(View v) {
                TurnONSteer(); //uruchom funkcję wysyłającą znaki do sterownika
                SteerFlag=true; //aktywuj flagę skręcania osią
                ActBasisflag=false; //pozstałe flagi pozostają dezaktywowane
                ActGripflag =false;
                ActArm1flag=false;
                ActArm2flag=false;
                ActArm3flag=false;
            }
        });

        SlowBtn.setOnClickListener(new View.OnClickListener() { //aktywuj po naciśnieciu na przycisk
            @Override
            public void onClick(View v) {
                Slow(); //wywołaj funkcję wysyłającą znaki
            }
        });

        FastBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fast();
            }
        });



        StraigthBtn.setOnTouchListener(new View.OnTouchListener() { //aktywuj funkcję jeżeli przycisk jest trzymany
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction()== MotionEvent.ACTION_DOWN) //jeżeli przyciks jest trzymany
                {

                    TurnONStraight(); //wywołaj funkcję pozwalającą na ruch platformy
                }else if (event.getAction()== MotionEvent.ACTION_UP) //jeżeli przycisk będzie puszczony
                {
                    Stop(); //wywołaj funkcje zatrzymującą platformę
                }
                return false;
            }
        });


        BackBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction()==MotionEvent.ACTION_DOWN)
                {

                    TurnONBack();
                }else if (event.getAction()==MotionEvent.ACTION_UP)
                {
                    Stop();
                }
                return false;
            }
        });

        ResetBtn.setOnClickListener(new View.OnClickListener() {//aktywuj w momencie kliknięcia
            @Override
            public void onClick(View v) {
                TurnONReset();//wywołaj funkcję Resetu

            }
        });


        GripAngle.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() { //funkcja akytywowana w momencie zmiany położenia suwaka odpowiadającego za chwytak
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if ((fromUser==true)&&(ActGripflag==true)); //aktywuj jeżeli zmiana położenia była spowodowana przez użytkownika i została aktywowana flaga chwytaka
                {
                  lev.setText(String.valueOf(progress)); //wypisz pozycję suwaka w etykiecie tekstowej na ekranie
                  try
                  {
                      BTsocket.getOutputStream().write(String.valueOf(progress).getBytes()); //wyslij pozycję chwytaka przez strumień wychodzący
                  }
                  catch (IOException e)
                  {
                      MSG("Wystąpił błąd połączenia"); //wyświetl komunikat w razie niepowodzenia
                  }

                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        Arm1Angle.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if((fromUser==true)&&(ActArm1flag==true));
                {
                    lev1.setText(String.valueOf(progress));
                    try
                    {
                        BTsocket.getOutputStream().write(String.valueOf(progress).getBytes());
                    }
                    catch (IOException e)
                    {
                        MSG("Wystąpił błąd połączenia");
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        Arm2Angle.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if((fromUser==true)&&(ActArm2flag==true));
                {
                    lev2.setText(String.valueOf(progress));
                    try
                    {
                        BTsocket.getOutputStream().write(String.valueOf(progress).getBytes());
                    }
                    catch (IOException e)
                    {
                        MSG("Wystąpił błąd połączenia");
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        Arm3Angle.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if((fromUser==true)&&(ActArm3flag==true));
                {
                    lev3.setText(String.valueOf(progress));
                    try
                    {
                        BTsocket.getOutputStream().write(String.valueOf(progress).getBytes());
                    }
                    catch (IOException e)
                    {
                        MSG("Wystąpił błąd połączenia");
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        BasisAngle.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if((fromUser==true)&&(ActBasisflag==true));
                {
                    lev4.setText(String.valueOf(progress));
                    try
                    {
                        BTsocket.getOutputStream().write(String.valueOf(progress).getBytes());
                    }
                    catch (IOException e)
                    {
                        MSG("Wystąpił błąd połączenia");
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        SteerAngle.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() { //funkcja aktywowana w momencie zmiany położenia suwaka
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if((fromUser==true)&&(SteerFlag==true)); //jeżeli zmiana została wywołana przez użytkownika i została aktywowana flaga
                {
                    lev5.setText(String.valueOf(progress));//wyświetl pozycję suwaka w etykiecie tekstowej
                    try
                    {
                        BTsocket.getOutputStream().write(String.valueOf(progress).getBytes()); //wyślij pozycję suwaka przez strumień wychodzący
                    }
                    catch (IOException e)
                    {
                        MSG("Wystąpił błąd połączenia"); //w przypadku niepowodzenia wyświetl komunikat
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });



    }

    private void MSG(String p)
    {
        Toast.makeText(getApplicationContext(),p,Toast.LENGTH_LONG).show();
    }

    private void Disconnect() //funkcja wywoływana po wciśnieciu przycisku rozłącz
    {
        if(BTsocket!=null) //jeżeli gniazdo istnieje
        {
         try
         {
             BTsocket.close(); //zamknij gniazdo
         }
         catch (IOException e)
         {
             MSG("Wystąpił błąd połączenia"); //wyświetl komunikat
         }
        }
    }

    private void TurnONGrip() //funkcja wysyłająca dwie litery sterownikowi aby ten wykonywał kod do sterowania chwytakiem
    {
        if(BTsocket!=null) //jeżeli gniazdo Bluetooth zostało utworzone
        {
            try //wykonaj próbę wysłania
            {
                BTsocket.getOutputStream().write("GO".toString().getBytes()); //wyślij przez strumień wychodzący dwie litery
            }
            catch(IOException e) //ta część wykonywana jest w momencie kiedy następi zakłócenie połączenia i nie zostanie wysłana informacja
            {
                MSG("Wystąpił błąd połączenia");
            }
        }
    }


    private void TurnONArm1()
    {
        if (BTsocket!=null)
        {
            try
            {
                BTsocket.getOutputStream().write("AO".toString().getBytes());
            }
            catch (IOException e)
            {
                MSG("Wystąpił błąd połączenia");
            }
        }
    }

    private void TurnONArm2()
    {
        if (BTsocket!=null)
        {
            try
            {
                BTsocket.getOutputStream().write("BO".toString().getBytes());
            }
            catch (IOException e)
            {
                MSG("Wystąpił błąd połączenia");
            }
        }
    }


    private void TurnONArm3()
    {
        if (BTsocket!=null)
        {
            try
            {
                BTsocket.getOutputStream().write("CO".toString().getBytes());
            }
            catch (IOException e)
            {
                MSG("Wystąpił błąd połączenia");
            }
        }
    }


    private void TurnONBasis()
    {
        if (BTsocket!=null)
        {
            try
            {
                BTsocket.getOutputStream().write("DO".toString().getBytes());
            }
            catch (IOException e)
            {
                MSG("Wystąpił błąd połączenia");
            }
        }
    }


    private void TurnONSteer() //funkcja wywoływana przez naciśniecie przycisku "Skręcaj"
    {
        if (BTsocket!=null) //czy gnaizdo Bluetooth zostało utworzone
        {
            try //wykonaj próbę wysłania znaków
            {
                BTsocket.getOutputStream().write("ST".toString().getBytes()); //wyślij znaki przez strumień wychodzący
            }
            catch (IOException e) //w przypadku niepowdzenia
            {
                MSG("Wystąpił błąd połączenia"); //wyświetl krótki komunikat
            }
        }
    }

    private void Slow() //funkcja wywoływana przez przycisk "Wolno"
    {
        if (BTsocket!=null)//czy gniazdo Bluetooth zostało utworzone
        {
            try //wykonaj próbę wysłania znaków
            {
                BTsocket.getOutputStream().write("SLO".toString().getBytes()); //wyślij znaki
            }
            catch (IOException e)
            {
                MSG("Wystąpił błąd połączenia");//w razie niepowodzenia wyświetl komunikat
            }
        }
    }

    private void Fast()
    {
        if (BTsocket!=null)
        {
            try
            {
                BTsocket.getOutputStream().write("FST".toString().getBytes());
            }
            catch (IOException e)
            {
                MSG("Wystąpił błąd połączenia");
            }
        }
    }

    private void TurnONStraight()//wywołaj funkcję trzymając przycisk "Jedź do przodu"
    {
        if ((BTsocket!=null))
        {
            try
            {
                BTsocket.getOutputStream().write("RUN".toString().getBytes());//wyślij znaki
            }
            catch (IOException e)
            {
                MSG("Wystąpił błąd połączenia");//w przypadku niepowodzenia wyświetl komunikat
            }
        }
    }

    private void Stop()//wywołaj funkcję puszczająć przycisk "Jedź do przodu" lub "Jedź do tyłu"
    {
        if (BTsocket!=null)
        {
            try
            {
                BTsocket.getOutputStream().write("STO".toString().getBytes());
            }
            catch (IOException e)
            {
                MSG("Wystąpił błąd połączenia");
            }
        }
    }

    private void TurnONBack()
    {
        if (BTsocket!=null)
        {
            try
            {
                BTsocket.getOutputStream().write("BACK".toString().getBytes());
            }
            catch (IOException e)
            {
                MSG("Wystąpił błąd połączenia");
            }
        }
    }

    private void TurnONReset()//wywołaj funkcję po naciśnieciu przycisku Reset
    {
        if (BTsocket!=null)
        {
            try
            {
                BTsocket.getOutputStream().write("RST".toString().getBytes());//wyślij ciąg znaków przez strumień wychodzący
            }
            catch (IOException e)
            {
                MSG("Wystąpił błąd połączenia");//w przypadku niepowdzenia wyświetl komunikat
            }
        }
    }

    private class ConnectBT extends AsyncTask<Void, Void, Void> //funkcja pozwalająca na połączenie z innym urządzneiem
    {
        private boolean ConnectSucces=true; //ustawienie flagi

        @Override
        protected void onPreExecute() //ta cześć określa co się dzieje za nim zostanie nawiązane połączenie
        {
            progress= ProgressDialog.show(Control.this,"Łączenie","Proszę czekać"); //wyświetlenie okienka "Łączenie" i komunikatu "Proszę czekać"

        }
        @Override
        protected Void doInBackground(Void... devices) //kiedy wyświetlane jest okienko "Łączenie" ta część wykonywana jest w tle
        {
            try //próba nawiązania połączenia
            {
               if(BTsocket==null|| !IsBTConnected) // jeżeli  nie zostało utworzone gniazdo lub nie ma połączenia bluetooth
               {
                   mBluetooth = BluetoothAdapter.getDefaultAdapter(); //uzyskaj domyślny adapter wbudowanego modułu bluetooth
                   BluetoothDevice myDevice = mBluetooth.getRemoteDevice(address); //uzyskaj adres urządzenia
                   BTsocket = myDevice.createInsecureRfcommSocketToServiceRecord(myUUID); // ustawienie niezabezpieczonego połączenia wychodzącego w standardzie RFCOMM
                   BluetoothAdapter.getDefaultAdapter().cancelDiscovery();// zakończy możliwość wykrywania urządzenia
                   BTsocket.connect(); //połącz
               }
            }
            catch (IOException e) //ustawienie wyjątku w przypadku niepowodzenia połączenia
            {
                ConnectSucces=false; //zmień wartość flagi
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result)//wykonuje się po próbie połączenia
        {
            super.onPostExecute(result);
            if(!ConnectSucces) //sprawdzenie czy udało się połączyć
            {
                MSG("Połączenie nie powiodło się"); //wyświetla krótką wiadomość na ekranie
                finish(); //wraca do poprzedniej aktywności
            }
            else
            {
                MSG("Połączono");
                IsBTConnected = true;
            }
            progress.dismiss(); // wyłącza okno "Łączenie"
        }
    }
}
