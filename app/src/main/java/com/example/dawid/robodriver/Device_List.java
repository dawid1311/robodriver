package com.example.dawid.robodriver;
//wykorzystywane bibliotek
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Set;

public class Device_List extends AppCompatActivity {

    Button paired; //definicja obiektu klasy button
    ListView deviceList; // definicja obiektu klasy ListView

    private BluetoothAdapter mBluetooth = null; // definicja obiektu klasy Bluetooth Adapter pozwalający na obsługę Bluetooth
    private Set<BluetoothDevice> pairedDevices; // definicja zmienniej odnąszacej sie do sparowanych urządzeń
    private OutputStream outStream=null; // definicja strumienia wyjściowego danych
    public static String EXTRA_ADDRESS="device_address"; //zapis adresu urządzenia



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device__list);

        paired= (Button)findViewById(R.id.button); //powiązanie obiektów interfejsu do elementów interfejsu umieszczonych w app designerze
        deviceList= (ListView)findViewById(R.id.list_view);

        mBluetooth=BluetoothAdapter.getDefaultAdapter(); //wywołanie wbudowanego modułu Bluetooth

        if(mBluetooth==null){ //sprawdzenie czy urządzenie posiada bluetooth
            Toast.makeText(getApplicationContext(), "Urządzenie nie wpsiera Bluetooth", Toast.LENGTH_LONG).show(); //wyświetlenie krótkiego komuniaktu na erkanie
            finish(); //wyłączenie aplikacji
        }
        else if(!mBluetooth.isEnabled())
        {
            Intent turnBTon= new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE); // uruchomienie modułu bluetooth
            startActivityForResult(turnBTon, 1);

        }
        paired.setOnClickListener(new View.OnClickListener() { // ustawienie przycisku w tryb oczekiwania na wciśniecie
            @Override
            public void onClick(View v) { //jeżeli przycisk zostanie wciśniety uruchomiona zostanie funkcja wyświetlenia listy urządzeń sparowanych
                pairedDevicesList();
            }
        });
    }
    private void pairedDevicesList()
    {
        pairedDevices = mBluetooth.getBondedDevices(); //przypsianie do zmiennej urządzeń sparowanych z adapterem Bluetooth
        ArrayList list = new ArrayList(); //stworzenie nowej tablicy
        if (pairedDevices.size()>0) // warunek na utworzenie listy urządzeń gotowej do wyświetlenia
        {
            for(BluetoothDevice bt : pairedDevices){
                list.add(bt.getName()+"\n" +bt.getAddress()); // tworzenie listy urządzeń z ich nazwami i adresami
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Brak sparowanych urządzeń", Toast.LENGTH_LONG).show(); //wyswietlenie komunikatu na ekranie
        }
        final ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1,list); //umożliwienie aby każdy element listy działał jako przycisk
        deviceList.setAdapter(adapter);
        deviceList.setOnItemClickListener(mListClickListener); //wywołanie metody w momencie kliknięcia na urządzenie
    }
    private AdapterView.OnItemClickListener mListClickListener = new AdapterView.OnItemClickListener() { //funkcja pozwalająca na uruchomienie kolejenej aktywności po kliknięciu na element listy
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
          String info=((TextView)view).getText().toString(); //uzyskanie adresu MAC i 17 ostanich znaków w widoku
          String address = info.substring(info.length()-17);

          Intent in = new Intent(Device_List.this, Control.class); //stworzenie intencji do uruchomienia kolejnej aktywności
          in.putExtra(EXTRA_ADDRESS, address); //zmiana aktywności
          startActivity(in);
        }
    };
}
